package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.lang.model.util.ElementScanner14;

import java.util.Random;

public class testing {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public String ComputerChoice() {
        int random_num;
        String computer_choice = "";
        Random random_choice = new Random();
        random_num = random_choice.nextInt(3) + 1;
        if (random_num == 1) {
            computer_choice = "rock";
        } else if (random_num == 2) {
            computer_choice = "paper";
        } else if (random_num == 3) {
            computer_choice = "scissors";
        }
        return computer_choice;
    }

    public boolean winner(String choice1, String choice2) {
        if (choice1 == "paper") {
            return (choice2 == "rock");

        } else if (choice1 == "scissors") {
            return (choice2 == "paper");
        } else {
            return (choice2 == "scissors");
        }
    }

    public String continuePlaying(String answer) {
        String farewell = "Bye Bye :)";
        if (answer.equals("n")) {
            return farewell;
        } else
            return null;
    }

    public void run() {
        System.out.println("Lets play round " + roundCounter);
        String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");

        System.out.println("Human chose " + humanChoice + " computer chose " + ComputerChoice());

        String continuePlaying = readInput("Do you wish to continue playing? (y/n)?");
        System.out.println(continuePlaying(continuePlaying));
    }

    /**
     * Reads input from console with given prompt
     * 
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
