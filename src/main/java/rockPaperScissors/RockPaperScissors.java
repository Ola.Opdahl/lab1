package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public String random_choice() {
        int random_num;
        String Computer_choice = "";
        Random random_rps = new Random();
        random_num = random_rps.nextInt(3) + 1;
        if (random_num == 1) {
            Computer_choice = "rock";
        }
        if (random_num == 2) {
            Computer_choice = "paper";
        }
        if (random_num == 3) {
            Computer_choice = "scissors";
        }
        return Computer_choice;

    }

    public Boolean is_winner(String PlayerMove, String ComputerMove) {
        if (PlayerMove.equals("rock")) {
            return ComputerMove.equals("scissors");
        } else if (PlayerMove.equals("paper")) {
            return ComputerMove.equals("rock");
        } else if (PlayerMove.equals("scissors")) {
            return ComputerMove.equals("paper");
        } else
            return false;

    }

    public void run() {
        String CPU_choice = random_choice();
        System.out.println("Let's play round " + roundCounter);
        String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
        if (rpsChoices.contains(humanChoice) == false) {
            String humanChoice2 = readInput("I do not understand " + humanChoice + ". Could you try again?");
            humanChoice = humanChoice2;
        }

        if (is_winner(humanChoice, CPU_choice)) {
            System.out
                    .println("Human chose " + humanChoice + ", computer chose " + CPU_choice + ". Human Wins!");
            humanScore += 1;
        } else if (humanChoice.equals(CPU_choice)) {
            System.out.println("Human chose " + humanChoice + ", computer chose " + CPU_choice + ". It's a tie!");
        } else if (is_winner(CPU_choice, humanChoice)) {
            System.out
                    .println("Human chose " + humanChoice + ", computer chose " + CPU_choice + ". Computer Wins!");
            computerScore += 1;
        }
        System.out.println("Score: human " + humanScore + ", computer " + computerScore);
        String play_again = readInput("Do you wish to continue playing? (y/n)? ");

        if (play_again.equals("y")) {
            roundCounter += 1;
            run();
        } else {
            System.out.println("Bye bye :)");
        }
    }

    /**
     * Reads input from console with given prompt
     * 
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
